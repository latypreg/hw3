package com.masteringselenium.tests;

import com.masteringselenium.DriverBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

public class RedmineTests extends DriverBase {

    @Test
    public void step1_successfulLoginTest() {
        driver.get("http://demo.redmine.org");
        //site takes my local language settings, so "Sign in" button is "Войти" for me
        driver.findElement(By.linkText("Войти")).click();

        waitForElement(driver, By.id("username"));

        driver.findElement(By.id("username"))
                .sendKeys("Regina");
        driver.findElement(By.id("password"))
                .sendKeys("qwerty");
        driver.findElement(By.name("login")).click();

        waitForElement(driver, By.id("loggedas"));

        WebElement loggedAs = driver.findElement(By.id("loggedas"));

        Assert.assertNotNull(loggedAs);
        Assert.assertTrue(loggedAs.isDisplayed());

        driver.findElement(By.linkText("Выйти")).click();
    }


    @Test
    public void step2_unsuccessfulLoginTest() {
        //site takes my local language settings, so "Sign in" button is "Войти" for me
        driver.findElement(By.linkText("Войти")).click();

        waitForElement(driver, By.id("username"));

        driver.findElement(By.id("username"))
                .sendKeys("Regina");
        driver.findElement(By.id("password"))
                .sendKeys("NOTqwerty");
        driver.findElement(By.name("login")).click();

        waitForElement(driver,By.id("flash_error"));

        WebElement flashError = driver.findElement(By.id("flash_error"));

        Assert.assertNotNull(flashError);
        Assert.assertTrue(flashError.isDisplayed());
    }
}